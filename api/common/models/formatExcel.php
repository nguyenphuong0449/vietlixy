<?php
/**
 * Created by PhpStorm.
 * User: hungluong
 * Date: 5/24/17
 * Time: 9:34 AM
 */

namespace common\models;


use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class formatExcel
{
    public static function copyRows($sheet,$srcRow,$dstRow,$height,$width) {
        for ($row = 0; $row < $height; $row++) {
            for ($col = 0; $col < $width; $col++) {
                $cell = $sheet->getCellByColumnAndRow($col, $srcRow + $row);
                $style = $sheet->getStyleByColumnAndRow($col, $srcRow + $row);
                $dstCell = Cell::stringFromColumnIndex($col) . (string)($dstRow + $row);
                $sheet->setCellValue($dstCell, $cell->getValue());
                $sheet->duplicateStyle($style, $dstCell);
            }

            $h = $sheet->getRowDimension($srcRow + $row)->getRowHeight();
            $sheet->getRowDimension($dstRow + $row)->setRowHeight($h);
        }

        foreach ($sheet->getMergeCells() as $mergeCell) {
            $mc = explode(":", $mergeCell);
            $col_s = preg_replace("/[0-9]*/", "", $mc[0]);
            $col_e = preg_replace("/[0-9]*/", "", $mc[1]);
            $row_s = ((int)preg_replace("/[A-Z]*/", "", $mc[0])) - $srcRow;
            $row_e = ((int)preg_replace("/[A-Z]*/", "", $mc[1])) - $srcRow;

            if (0 <= $row_s && $row_s < $height) {
                $merge = $col_s . (string)($dstRow + $row_s) . ":" . $col_e . (string)($dstRow + $row_e);
                $sheet->mergeCells($merge);
            }
        }
    }

    /**
     * @param $activeSheet Worksheet
     */
    public static function setDateValue($activeSheet, $range, $cell, $value = "2017-12-31", $format = "d/m/Y"){
        if($value != ""){
            $activeSheet
                ->getStyle($range)
                ->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_TEXT);

            $activeSheet->setCellValue($cell,$value!=""?date($format,strtotime($value)):"", true);
        }
    }

    /**
     * @param $activeSheet Worksheet
     * @param $cell string
     * @param string $value
     * @param string $format
     */
    public static function setDateValue2($activeSheet, $cell, $value = "2017-12-31", $format = "d/m/Y"){
        if($value != ""){
            $dateTimeNow = strtotime($value);
            $activeSheet
                ->setCellValue($cell, NumberFormat::PHPToExcel( $dateTimeNow ));
            $activeSheet
                ->getStyle($cell)
                ->getNumberFormat()
                ->setFormatCode('dd/mm/yyyy');
        }
    }

    /**
     * @param $activeSheet Worksheet
     */
    public static function setPositionImage($activeSheet, $pathImage, $height = 0, $width = 0, $setResizeProportional = false,
                                            $coordinates = "A1", $name = "THDG TV", $description = "THDG TV", $offetX = 0, $offetY = 0){
        $objDrawing = new Drawing();
        $objDrawing->setName($name);
        $objDrawing->setDescription($description);
        $objDrawing->setPath($pathImage);
        $objDrawing->setHeight($height);
        $objDrawing->setWidth($width);
        $objDrawing->setCoordinates($coordinates);
        $objDrawing->setOffsetX($offetX);
        $objDrawing->setOffsetY($offetY);

        $objDrawing->setResizeProportional($setResizeProportional);
        $objDrawing->setWorksheet($activeSheet);
    }

    /**
     * @param $acticeSheet Worksheet
     */
    public static function setFontBold($acticeSheet, $range){
        $acticeSheet->getStyle($range)
            ->applyFromArray([
                'font' => [
                    'bold' => true
                ]
            ]);
    }

    /**
     * @param $acticeSheet Worksheet
     */
    public static function setFontItalic($acticeSheet, $range){
        $acticeSheet->getStyle($range)
            ->applyFromArray([
                'font' => [
                    'italic' => true
                ]
            ]);
    }

    /**
     * @param $activeSheet Worksheet
     */
    public static function alignCenterText($activeSheet, $range){
        $activeSheet->getStyle($range)->getAlignment()->applyFromArray(
            array('horizontal' => Alignment::HORIZONTAL_CENTER,)
        );
    }

    /**
     * @param $activeSheet Worksheet
     * @param $columnStart string
     * @param $columnEnd string
     */
    public static function setAutoWidthColumnd($activeSheet, $columnStart = 'A', $columnEnd = 'Z'){
        foreach(range($columnStart,$columnEnd) as $columnID)
        {
            $activeSheet->getColumnDimension($columnID)->setAutoSize(true);
        }
    }

    /**
     * @param $activeSheet Worksheet
     * @param $columnStart string
     * @param $columnEnd string
     * @param $width int
     */
    public static function setWidthColumn($activeSheet, $columnStart = 'A', $columnEnd = 'A', $width = -1){
        foreach(range($columnStart,$columnEnd) as $columnID)
        {
            $activeSheet->getColumnDimension($columnID)->setWidth($width);
        }
    }

    /**
     * @param $activeSheet Worksheet
     * @param $range string
     */
    public static function setWrapText($activeSheet, $range){
        $activeSheet->getStyle($range)->getAlignment()->setWrapText(true);
    }

    /**
     * @param $activeSheet Worksheet
     * @param $range string
     */
    public static function alignVerticalCenter($activeSheet, $range){
        $activeSheet->getStyle($range)->getAlignment()->applyFromArray(
            array('vertical' => Alignment::VERTICAL_CENTER,)
        );
    }

    /**
     * @param $activeSheet Worksheet
     * @param $range string
     */
    public static function alignHorizontalLeft($activeSheet, $range){
        $activeSheet->getStyle($range)->getAlignment()->applyFromArray(
            array('horizontal' => Alignment::HORIZONTAL_LEFT)
        );
    }

    /**
     * @param $activeSheet Worksheet
     * @param $range string
     */
    public static function setFontSize($activeSheet, $range, $size){
        $activeSheet->getStyle($range)->applyFromArray([
            'font' => [
                'size' => $size
            ]
        ]);
    }

    /**
     * @param $activeSheet Worksheet
     */
    public static function setHeightRow($activeSheet, $row, $height = 40){
        $activeSheet->getRowDimension($row)->setRowHeight($height);
    }

    /**
     * @param $activeSheet Worksheet
     * @param $range string
     */
    public static function setBorder($activeSheet, $range){
        $activeSheet->getStyle($range)->applyFromArray([
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => Border::BORDER_THIN
                )
            )
        ]);
    }

    /**
     * @param $activeSheet Worksheet
     */
    public static function setFontFamily($activeSheet, $range, $fontSize = 12, $fontFamily='Times New Roman'){
        $activeSheet->getStyle($range)->getFont()->setName($fontFamily)->setSize($fontSize);
    }

    /**
     * @param $activeSheet Worksheet
     * @param $range string
     * @param $colorRGB string
     */
    public static function setBgColor($activeSheet, $range, $colorRGB){
        $activeSheet
            ->getStyle($range)->getFill()->applyFromArray(array(
                'type' => Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => $colorRGB
                )
            ));
    }

    public static function setColor($activeSheet, $range, $colorRGB){
        $activeSheet
            ->getStyle($range)->getFont()->getColor()->setRGB($colorRGB);
    }

    public static function setWordWrap($activeSheet, $range){
        $activeSheet
            ->getStyle($range)->getAlignment()->setWrapText(true);
    }

    /**
     * @param $objWorkSheet
     * @param $index IOFactory
     */
    public static function deleteSheet($objWorkSheet, $index){
        $objWorkSheet->removeSheetByIndex($index);
    }
}
