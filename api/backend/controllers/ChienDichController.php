<?php

namespace backend\controllers;

use backend\models\ChienDich;
use backend\models\DanhMuc;
use backend\models\Network;
use backend\models\Postback;
use backend\models\QuanLyChienDich;
use backend\models\ChienDichPostback;
use backend\models\TrangThaiChienDich;
use common\models\myAPI;
use common\models\User;
use PhpOffice\PhpSpreadsheet\Shared\OLE\PPS;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use Yii;

class ChienDichController extends CoreApiController
{
    public function behaviors()
    {
        $arr_action = ['get-data', 'get-chien-dich-postback', 'save', 'load', 'load-option', 'delete', 'delete-chien-dichs', 'get-chien-dichs', 'update-chien-dich',
            'chuyen-trang-thai-nhieu-chien-dich', 'chuyen-noi-bat-nhieu-chien-dich', 'update-thong-tin', 'get-link', 'xem-chi-tiet', 'xem-chi-tiet-post-back', 'copy-offer'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name = strtolower(str_replace('action', '', $action->id));
                    $data = myAPI::getDataPost();
                    $uid = $data['uid'];
                    return myAPI::isAccess2('ChienDich', $action_name, $uid);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
        ];
    }

    /** get-data */
    public function actionGetData()
    {
        $query = QuanLyChienDich::find()->andFilterWhere(['active' => 1]);
        if (isset($this->dataPost['fieldsSearch'])) {
            if (count($this->dataPost['fieldsSearch']['value']) > 0) {
                $arrFieldSearch = ['title', 'vi_tri_hien_thi'];
                foreach ($arrFieldSearch as $item) {
                    if ((isset($this->dataPost['fieldsSearch']['value'][$item])))
                        if ($this->dataPost['fieldsSearch']['value'][$item] != '')
                            $query->andFilterWhere(['like', $item, $this->dataPost['fieldsSearch']['value'][$item]]);
                }
                if ((isset($this->dataPost['fieldsSearch']['value']['noi_bat']['type']))){
                    if ($this->dataPost['fieldsSearch']['value']['noi_bat']['type'] >= 0)
                        $query->andFilterWhere(['<>','noi_bat',(($this->dataPost['fieldsSearch']['value']['noi_bat']['type']==0)?1:0)]);
                }

                $arrOptionsSearch = ['trang_thai', 'danh_muc_id', 'network_id'];
                foreach ($arrOptionsSearch as $item){
                    if ((isset($this->dataPost['fieldsSearch']['value'][$item]['type']))){
                        if ($this->dataPost['fieldsSearch']['value'][$item]['type'] != '')
                            $query->andFilterWhere(['like',$item, $this->dataPost['fieldsSearch']['value'][$item]['type']]);
                    }
                }

            }
        }
        $totalCount = $query->count();
        $data = $query
            ->select(['id', 'title', 'chi_phi_chien_dich', 'trang_thai', 'vi_tri_hien_thi', 'noi_bat',
                'hoa_hong_tu_net', 'image', 'track_link', 'type_hoa_hong', 'Capped', 'danh_muc_id', 'network_id', 'network'])
            ->offset(($this->dataPost['offset'] - 1) * $this->dataPost['perPage'])
            ->orderBy(['created' => SORT_DESC])
            ->limit($this->dataPost['limit'])
            ->all();

        return [
            'results' => $data,
            'rows' => $totalCount,
        ];
    }

    /** get-chien-dich-postback */
    public function actionGetChienDichPostback()
    {
        $query = ChienDichPostback::find()->andWhere(['active' => 1]);
        $tongNet = 0;
        $tongNhanDuoc = 0;

        if (isset($this->dataPost['fieldsSearch'])) {
            if (count($this->dataPost['fieldsSearch']['value']) > 0 && !($this->dataPost['fieldsSearch']['value']['title'] == '' && $this->dataPost['fieldsSearch']['value']['hoten'] == ''
                    && $this->dataPost['fieldsSearch']['value']['note_status'] ==''&& $this->dataPost['fieldsSearch']['value']['ngay_thuc_hien_tu'] == ''&& $this->dataPost['fieldsSearch']['value']['ngay_thuc_hien_den']=='')) {
                $arrFieldSearch = ['title', 'hoten'];
                foreach ($arrFieldSearch as $item) {
                    if (isset($this->dataPost['fieldsSearch']['value'][$item])) {
                        if ($this->dataPost['fieldsSearch']['value'][$item] != '')
                            $query->andFilterWhere(['like', $item, $this->dataPost['fieldsSearch']['value'][$item]]);
                    }
                }

                if ((isset($this->dataPost['fieldsSearch']['value']['note_status']['key'])))
                    if ($this->dataPost['fieldsSearch']['value']['note_status']['key'] != '')
                        $query->andFilterWhere(['note_status' => $this->dataPost['fieldsSearch']['value']['note_status']['key']]);

                if ($this->dataPost['fieldsSearch']['value']['ngay_thuc_hien_tu'] != '')
                    $query->andFilterWhere(['>=', 'date(created)', date('Y-m-d', strtotime($this->dataPost['fieldsSearch']['value']['ngay_thuc_hien_tu']))]);
                if ($this->dataPost['fieldsSearch']['value']['ngay_thuc_hien_den'] != '')
                    $query->andFilterWhere(['<=', 'date(created)', date('Y-m-d', strtotime($this->dataPost['fieldsSearch']['value']['ngay_thuc_hien_den']))]);

                $tongNet += $query->sum('chi_phi_chien_dich');
                $tongNhanDuoc += $query->sum('so_tien_nhan_duoc');
            }
        }
        if (!User::isViewAll($this->dataPost['uid'])) {
            $query->andWhere(['nguoi_thuc_hien_id' => $this->dataPost['uid']]);
        }
        if (isset($this->dataPost['userId'])) {
            $query->andWhere(['nguoi_thuc_hien_id' => $this->dataPost['userId']]);
        }
        $totalCount = $query->count();
        $query = $query->select([
                'id',
                'title', 'chi_phi_chien_dich', 'utm_source',
                'note_status', 'so_tien_nhan_duoc', 'image',
                'nguoi_thuc_hien', 'ngay_thuc_hien', 'created', 'hoten'])
            ->orderBy(['id' => SORT_DESC])
            ->offset(($this->dataPost['offset'] - 1) * $this->dataPost['perPage'])
            ->limit($this->dataPost['limit'])
            ->all();
        if (isset($this->dataPost['fieldsSearch'])) {
            if (count($this->dataPost['fieldsSearch']['value']) == 0 || ($this->dataPost['fieldsSearch']['value']['title'] == '' && $this->dataPost['fieldsSearch']['value']['hoten'] == ''
            && $this->dataPost['fieldsSearch']['value']['note_status'] ==''&& $this->dataPost['fieldsSearch']['value']['ngay_thuc_hien_tu'] == ''&& $this->dataPost['fieldsSearch']['value']['ngay_thuc_hien_den']=='')) {
                foreach ($query as $item) {
                    $tongNet += $item->chi_phi_chien_dich;
                    $tongNhanDuoc += $item->so_tien_nhan_duoc;
                }
            }
        }

        return [
            'results' => $query,
            'rows' => $totalCount,
            'tongNet' => $tongNet,
            'tongNhanDuoc' => $tongNhanDuoc,
            'isViewAll' => User::isViewAll($this->dataPost['uid']),
        ];
    }

    /** save */
    public function actionSave()
    {
        if (isset($this->dataPost['type_hoa_hong'])){
            if ($this->dataPost['type_hoa_hong']=='')
                throw new HttpException(500, 'Vui lòng chọn loại hoa hồng');
        }
        if (isset($this->dataPost['danh_muc_id'])){
            if ($this->dataPost['danh_muc_id']=='')
                throw new HttpException(500, 'Vui lòng chọn loại chiến dịch');
        }
        if ($this->dataPost['id'] != '')
            $model = ChienDich::findOne($this->dataPost['id']);
        else {
            $model = new ChienDich();
        }

        foreach ($this->dataPost as $attr => $value) {
            if (!in_array($attr, ['auth', 'uid', 'url', 'domain', 'anhChienDich'])) {
                if (in_array($attr, ['vi_tri_hien_thi', 'hoa_hong_tu_net', 'chi_phi_chien_dich','link_video', 'Capped'])) {
                    $model->{$attr} = doubleval(str_replace(',', '', $this->dataPost[$attr]));
                } elseif (in_array($attr, ['network_id', 'category_id', 'device_id', 'type_hoa_hong'])) {
                    if (isset($this->dataPost[$attr])) {
                        if (isset($this->dataPost[$attr]['key'])){
                            if ($this->dataPost[$attr]['key'] != '')
                            $model->{$attr} = intval($this->dataPost[$attr]['key']);
                        }

                        if (isset($this->dataPost[$attr]['value'])){
                            if ($this->dataPost[$attr]['value'] != '')
                            $model->{$attr} = ($this->dataPost[$attr]['value']);
                        }
                    }
                } else
                    $model->{$attr} = $this->dataPost[$attr];
            }
            if (isset($this->dataPost['anhChienDich'])) {
                if ($this->dataPost['anhChienDich'] != '') {
                    foreach ($this->dataPost['anhChienDich'] as $index => $anhBanner) {
                        $arr_data_image = explode(',', $anhBanner);
                        $file = base64_decode($arr_data_image[1]);
                        $loai_file = explode('/', explode(';', $arr_data_image[0])[0])[1];
                        $fileName = time() . $index . myAPI::createCode($this->dataPost['title']) . '.' . $loai_file;

                        file_put_contents(dirname(dirname(__DIR__)) . '/images/' . $fileName, $file);

                        $model->image = 'https://app.vietlixi.com/api/images/'.$fileName;
                    }
                }
            }
        }
        $model->user_id = $this->dataPost['uid'];
        $model->noi_bat = 1;
        if(isset($this->dataPost['danh_muc_id'])){
            $this->dataPost['danh_muc_id'] = json_decode(json_encode($this->dataPost['danh_muc_id']), FALSE);
            foreach ($this->dataPost['danh_muc_id'] as $item){
                $model->danh_muc_id = $item->value;
                $model->isNewRecord = true;
                $model->id = null;
                if (!$model->save())
                    throw new HttpException(500, Html::errorSummary($model));
            }
            return [
                'content' => 'Thêm chiến dịch thành công',
            ];
        }else{
            throw new HttpException(500, "Vui lòng chọn ít nhất 1 danh mục");

        }

    }

    /** load */
    public function actionLoad()
    {
        $model = ChienDich::findOne($this->dataPost['chienDich']);
        if (is_null($model)) {
            throw new HttpException(500, 'Campain không tồn tại');
        }
        if ($model->network_id) {
            $model->network_id = ['key' => $model->network_id, 'label' => $model->network->Name];
        }
        if ($model->category_id) {
            $model->category_id = ['key' => $model->category_id, 'label' => $model->category->name];
        }
        if ($model->type_id) {
            $model->type_id = ['key' => $model->type_id, 'label' => $model->type->name];
        }
        if ($model->device_id) {
            $model->device_id = ['key' => $model->device_id, 'label' => $model->device->name];
        }
        if ($model->type_hoa_hong) {
            $model->type_hoa_hong = ['value' => $model->type_hoa_hong, 'label' => $model->type_hoa_hong];
        }
        $model->Request = $model->Request == 1;
        $model->noi_bat = $model->noi_bat == 1;
        $model->active = $model->trang_thai == ChienDich::HOAT_DONG;
        $model->vi_tri_hien_thi = $model->vi_tri_hien_thi+1;


        return [
            'result' => $model
        ];
    }

    /** load-option */
    public function actionLoadOption()
    {
        $arr_network = [];
        $arr_category = [];
        $arr_type = [];
        $arr_device = [];

        $networks = Network::findAll(['active' => 1]);
        $categorys = DanhMuc::findAll(['type' => DanhMuc::CATEGORY, 'active' => 1]);
        $types = DanhMuc::findAll(['type' => DanhMuc::OFFER_TYPE, 'active' => 1]);
        $devices = DanhMuc::findAll(['type' => DanhMuc::OFFER_DEVICE, 'active' => 1]);
        $dich_vu = DanhMuc::findAll(['type' => DanhMuc::DICH_VU, 'active' => 1]);
        foreach ($networks as $network) {
            $arr_network[] = ['key' => $network->id, 'label' => $network->Name];
        }
        foreach ($categorys as $category) {
            $arr_category[] = ['key' => $category->id, 'label' => $category->name];
        }
        foreach ($types as $type) {
            $arr_type[] = ['key' => $type->id, 'label' => $type->name];
        }
        foreach ($devices as $device) {
            $arr_device[] = ['key' => $device->id, 'label' => $device->name];
        }
        foreach ($dich_vu as $dich_vus) {
            $arr_dich_vu[] = ['key' => $dich_vus->id, 'label' => $dich_vus->name];
        }

        return [
            'network' => $arr_network,
            'category' => $arr_category,
            'type' => $arr_type,
            'device' => $arr_device,
            'dichVu' => $arr_dich_vu,
        ];
    }

    /** delete */
    public function actionDelete()
    {
        $model = ChienDich::findOne($this->dataPost['chienDich']);
        if (!is_null($model)) {
            $model->updateAttributes(['active' => 0]);

            return [
                'title' => 'Xóa campaign',
                'content' => 'Đã xóa campaign thành công',
            ];
        } else {
            throw new HttpException(500, 'Campaign không tồn tại');
        }
    }

    /** delete-chien-dichs */
    public function actionDeleteChienDichs()
    {
        if (count($this->dataPost['chienDichs']) > 0) {
            foreach ($this->dataPost['chienDichs'] as $item) {
                $chienDich = ChienDich::findOne($item['id']);
                if (!is_null($chienDich)) {
                    $chienDich->updateAttributes(['active' => 0]);
                }
            }
            return [
                'content' => 'Xóa chiến dịch thành công'
            ];
        } else {
            if (count($this->dataPost['chienDich']) == 0)
                throw new HttpException(500, 'Vui lòng chọn ít nhất 1 chiến dịch');
        }
    }

    /** get-chien-dichs */
    public function actionGetChienDichs()
    {
        $trangThai = [
            ['key' => ChienDich::HOAT_DONG, 'label' => ChienDich::HOAT_DONG],
            ['key' => ChienDich::KHONG_HOAT_DONG, 'label' => ChienDich::KHONG_HOAT_DONG],
        ];

        $noi_bat = [
            ['key' => 0, 'label' => ChienDich::KHONG_NOI_BAT],
            ['key' => 1, 'label' => ChienDich::NOI_BAT],
        ];

        $chienDich = [];
        foreach ($this->dataPost['chienDichDaChon'] as $item) {
            $chienDich[] = ChienDich::find()
                ->select(['id', 'title', 'trang_thai'])
                ->andFilterWhere(['id' => $item['id']])
                ->andFilterWhere(['active' => 1])
                ->one();

        }
        return [
            'chienDich' => $chienDich,
            'trangThai' => $trangThai,
            'noiBat' => $noi_bat,
        ];
    }

    /** update-thong-tin */
    public function actionUpdateThongTin()
    {
        if (isset($this->dataPost['chienDich']) > 0 && isset($this->dataPost['type'])) {
            $chien_dich = ChienDich::findOne($this->dataPost['chienDich']['id']);
            if ($this->dataPost['type'] == 'thay_vi_tri') {
                $chien_dich->updateAttributes(['vi_tri_hien_thi' => ($this->dataPost['chienDich']['vi_tri_hien_thi']-1)]);
            }
            if ($this->dataPost['type'] == 'thay_link') {
                $chien_dich->updateAttributes(['track_link' => ($this->dataPost['chienDich']['track_link'])]);
            }
            if (isset($this->dataPost['link_video'] )) {
                $chien_dich->updateAttributes(['link_video' => ($this->dataPost['link_video'])]);
            }
            if ($this->dataPost['type'] == 'thay_tien_thuong') {
                $chien_dich->updateAttributes([
                    'ti_le_hoan_tien' => doubleval(str_replace(',', '', $this->dataPost['chienDich']['ti_le_hoan_tien'])),
                ]);
                if (isset($this->dataPost['chienDich']['type_hoan_tien']['value'])){
                    if ($this->dataPost['chienDich']['type_hoan_tien']['value'] != ''){
                        $chien_dich->updateAttributes([
                            'type_hoan_tien' => $this->dataPost['chienDich']['type_hoan_tien']['value'],
                        ]);
                    }
                }
            }
            if ($this->dataPost['type'] == 'thay_hoa_hong') {
                $chien_dich->updateAttributes([
                    'hoa_hong_tu_net' => doubleval(str_replace(',', '', $this->dataPost['chienDich']['hoa_hong_tu_net'])),
                ]);
                if (isset($this->dataPost['chienDich']['hoa_hong_tu_net']['value'])){
                    if ($this->dataPost['chienDich']['hoa_hong_tu_net']['value'] != ''){
                        $chien_dich->updateAttributes([
                            'hoa_hong_tu_net' => $this->dataPost['chienDich']['hoa_hong_tu_net']['value'],
                        ]);
                    }
                }
            }
            if ($this->dataPost['type'] == 'thay_vi_tri') {
                return [
                    'title' => 'Thông báo',
                    'content' => 'Cập nhật thông tin thành công'
                ];
            }
            if ($this->dataPost['type'] == 'thay_link') {
                return [
                    'title' => 'Thông báo',
                    'content' => 'Cập nhật Track link thành công'
                ];
            }
            if ($this->dataPost['type'] == 'thay_tien_thuong') {
                return [
                    'title' => 'Thông báo',
                    'content' => 'Cập nhật tiền thưởng chiến dịch thành công'
                ];
            }
            if ($this->dataPost['type'] == 'thay_hoa_hong') {
                return [
                    'title' => 'Thông báo',
                    'content' => 'Cập nhật hoa hồng thành công'
                ];
            }
        }
    }

    /** get-link */
    public function actionGetLink()
    {
        $chien_dich = ChienDich::findOne($this->dataPost['id']);
        $uid = $this->dataPost['uid'];
        if (is_null($chien_dich)) {
            throw new HttpException(500, 'Chiến dịch không tồn tại');
        }
        Yii::$app->session->set('uid', $this->dataPost['uid']);

        if ($chien_dich->track_link) {
            $postback = Postback::find()->andWhere(['utm_source' => "{$uid}_{$chien_dich->id}"])
                ->andWhere(['in', 'trang_thai', [ChienDich::HOAT_DONG, ChienDich::KHONG_HOAT_DONG]])->one();
            if (!is_null($postback)) {
                return [
                    'title' => 'Đăng ký',
                    'content' => 'Bạn đã đăng ký chiến dịch này',
                ];
            }

            return [
                'title' => 'Đăng ký',
                'content' => Html::a('<i class="fa fa-cloud-download"></i> Nhấn vào đây để đăng ký!',
                    "http://localhost/vietlixi/api/service/redirect-link?chien_dich_id={$chien_dich->id}",
                    ['class' => 'text-primary', 'target' => '_blank'])
            ];
        } else {
            return [
                'title' => 'Đăng ký',
                'content' => Html::a('<i class="fa fa-cloud-download"></i> Nhấn vào đây để đăng ký!', '#', ['class' => 'text-primary'])
            ];
        }
    }

    public function actionXemChiTiet()
    {
        $model = QuanLyChienDich::find()
            ->select([
                'id', 'title','conversion_requirements',
                'category_id',
                'type_id', 'created',
                'track_link',
                'image',
                'hoa_hong_tu_net',
                'type_hoa_hong',
                'trang_thai',
                'chi_phi_chien_dich', 'danh_muc_id', 'ten_danh_muc',
                'network',
                'link_video'
            ])
            ->andFilterWhere(['id' => $this->dataPost['chienDich']])
            ->one();
        $network = Network::find()->select(['id as value','Name as label'])->andFilterWhere(['active'=>1])->createCommand()->queryAll();


        return [
            'chienDich' => $model,
            'network' => $network
        ];
    }

    public function actionXemChiTietPostBack()
    {
        $model = ChienDichPostback::find()
            ->select([
                'id', 'utm_source', 'status', 'reward','image',
                'trang_thai', 'hoa_hong', 'type_hoa_hong', 'so_tien_nhan_duoc',
                'loi_nhuan', 'created', 'updated', 'chien_dich_id', 'user_id',
                'active', 'note_status', 'chi_phi_chien_dich', 'title', 'ngay_thuc_hien',
                'trang_thai_postback', 'nguoi_thuc_hien', 'hoten'
            ])
            ->andFilterWhere(['utm_source' => $this->dataPost['utm_source']])
            ->one();
        $lich_su = ChienDichPostback::find()
            ->select(['note_status', 'ngay_thuc_hien'])
            ->andFilterWhere(['utm_source' => $this->dataPost['utm_source']])
            ->all();


        return [
            'chienDich' => $model,
            'lichSuTrangThai' => $lich_su
        ];
    }

    public function actionChuyenNoiBatNhieuChienDich()
    {
        if (isset($this->dataPost['chienDich'])) {
            if (!is_null($this->dataPost['chienDich'])) {
                $query = ChienDich::findOne(['id' => $this->dataPost['chienDich']]);
                $query->updateAttributes(['noi_bat' => $query->noi_bat == 1 ? 0 : 1]);
            }
        } else if (isset($this->dataPost['chienDichDaChon'])) {
            if (!is_null($this->dataPost['chienDichDaChon'])) {
                foreach ($this->dataPost['chienDichDaChon'] as $item) {
                    $query = ChienDich::findOne(['id' => $item['id']]);
                    $query->updateAttributes(['noi_bat' => $query->noi_bat == 1 ? 0 : 1]);
                }
            }
        }

        if (isset($query)) {
            if (!is_null($query)) {
                return [
                    'noiBat' => $query,
                ];
            }
        } else {
            throw new HttpException(500, 'Vui lòng chọn ít nhất 1 chiến dịch');
        }

    }

    public function actionChuyenTrangThaiNhieuChienDich()
    {
        if (isset($this->dataPost['chienDich'])) {
            if (!is_null($this->dataPost['chienDich'])) {
                $query = ChienDich::findOne(['id' => $this->dataPost['chienDich']]);
                $query->updateAttributes(['trang_thai' => $query->trang_thai == ChienDich::HOAT_DONG ? ChienDich::KHONG_HOAT_DONG : ChienDich::HOAT_DONG]);
            }
        } else if (isset($this->dataPost['chienDichDaChon'])) {
            if (!is_null($this->dataPost['chienDichDaChon'])) {
                foreach ($this->dataPost['chienDichDaChon'] as $item) {
                    $query = ChienDich::findOne(['id' => $item['id']]);
                    $query->updateAttributes(['trang_thai' => $query->trang_thai == ChienDich::HOAT_DONG ? ChienDich::KHONG_HOAT_DONG : ChienDich::HOAT_DONG]);
                }
            }
        }

        if (isset($query)) {
            if (!is_null($query)) {
                return [
                    'trangThai' => $query,
                ];
            }
        } else {
            throw new HttpException(500, 'Vui lòng chọn ít nhất 1 chiến dịch');
        }

    }

    /** save */
    public function actionUpdateChienDich()
    {
        if ($this->dataPost['id'] != '')
            $model = ChienDich::findOne($this->dataPost['id']);
        else {
            $model = new ChienDich();
        }
        $arr_update = ['type_hoa_hong', 'trang_thai'];
        foreach ($arr_update as $item){
            if (isset($this->dataPost[$item]['value'])){
                if ($this->dataPost[$item]['value'] != ''){
                    $model->updateAttributes([$item => $this->dataPost[$item]['value']]);
                }
            }  else {
                $model->updateAttributes([$item => $this->dataPost[$item]]);
            }
        }

        if (isset($this->dataPost['ten_danh_muc']['value'])){
            if ($this->dataPost['ten_danh_muc']['value'] != ''){
                $model->updateAttributes(['danh_muc_id' => $this->dataPost['ten_danh_muc']['value']]);
            }
        }  else {
            $model->updateAttributes(['danh_muc_id' => $this->dataPost['danh_muc_id']]);
        }
        if (isset($this->dataPost['network']['value'])){
            if ($this->dataPost['network']['value'] != ''){
                $model->updateAttributes(['network_id' => $this->dataPost['network']['value']]);
            }
        }  else {
            $netID = Network::findOne(['Name'=>$this->dataPost['network']]);
            if(!is_null($netID))
            $model->updateAttributes(['network_id' => $netID->id]);
        }

        if ($model->type_hoa_hong == 'Tiền mặt'){
            $hoa_hong = explode('.', $this->dataPost['hoa_hong_tu_net']);
            $hoa_hong = implode('', $hoa_hong);
            $model->updateAttributes(['hoa_hong_tu_net' => (int)$hoa_hong]);
        } else {
            $hoa_hong = explode(',', $this->dataPost['hoa_hong_tu_net']);
            $hoa_hong = implode('.', $hoa_hong);
            $model->updateAttributes(['hoa_hong_tu_net' => $hoa_hong]);
        }

        if (isset($this->dataPost['image'])){
            if ($this->dataPost['image'] != '')
            $model->updateAttributes(['image' => $this->dataPost['image']]);
        }

        $chi_phi = explode('.', $this->dataPost['chi_phi_chien_dich']);
        $chi_phi = implode('', $chi_phi);
        $model->updateAttributes(['chi_phi_chien_dich' => (int)$chi_phi]);

        $model->updateAttributes([
            'title' => $this->dataPost['title'],
            'track_link' => $this->dataPost['track_link'],
            'type_id' => $this->dataPost['type_id']
        ]);

        if ($model->save())
            return [
                'content' => 'Đã lưu thông tin chiến dịch thành công',
            ];
        else
            throw new HttpException(500, Html::errorSummary($model));
    }
    public function actionCopyOffer()
    {
        $id = intval($this->dataPost['id']);
        $offer = ChienDich::findOne($id);
        if(is_null($offer)){
            throw new HttpException(500,'Không tìm thấy chiến dịch này!');
        }
        $offer->isNewRecord = true;
        $offer->id = null;
        if(!$offer->save()){
            throw new HttpException(500,Html::errorSummary($offer));
        }
        return [
            'message'=>'Sao chép chiến dịch thành công!',
            'data'=>$offer,
        ];
    }
}
