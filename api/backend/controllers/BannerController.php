<?php
namespace backend\controllers;

use backend\models\Banner;
use backend\models\ImagesBanner;
use backend\models\QuanLyBanner;
use common\models\myAPI;
use common\models\User;
use Faker\Provider\Image;
use Yii;
use backend\models\DanhMuc;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

class BannerController extends CoreApiController
{
    public function behaviors()
    {

        $arr_action = ['get-all-data', 'load', 'save', 'delete', 'xoa-anh-banner'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    $data = myAPI::getDataPost();
                    $uid  = $data['uid'];
                    return myAPI::isAccess2('Banner', $action_name, $uid);
                }
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
        ];
    }
    // get-all-data
    public function actionGetAllData(){
        $query = QuanLyBanner::find();
        $totalCount = $query->count();
        if (isset($this->dataPost['fieldsSearch'])) {
            if (count($this->dataPost['fieldsSearch']['value']) > 0) {
                $arrFieldSearch = ['title', 'region', 'vi_tri'];
                foreach ($arrFieldSearch as $item) {
                    if ((isset($this->dataPost['fieldsSearch']['value'][$item])))
                        if ($this->dataPost['fieldsSearch']['value'][$item] != '')
                            $query->andFilterWhere(['like', $item, $this->dataPost['fieldsSearch']['value'][$item]]);
                }
            }
        }
        $data = $query
            ->offset(($this->dataPost['offset'] - 1) * $this->dataPost['perPage'])
            ->limit($this->dataPost['limit'])
            ->orderBy(['id' => SORT_DESC])
            ->andFilterWhere(['active' => 1])
            ->all();

        return [
            'results' => $data,
            'rows' => $totalCount
        ];
    }

    /** load */
    public function actionLoad(){
        /** @var Banner $user */
        $user = Banner::find()
            ->andFilterWhere(['id' => $this->dataPost['danh_muc']])
            ->andFilterWhere(['active' => 1])
            ->one();
        if(!is_null($user)){
            if($user->region != '')
                $user->region = ['key' => $user->region, 'label' => $user->region];
            return [
                'banner' => $user,
                'anhBanners' => ImagesBanner::findAll(['banner_id' => $user->id])
            ];
        }
        throw new HttpException(500, 'Không tìm thấy dữ liệu tương ứng');
    }

    /** save */
    public function actionSave(){
        if($this->dataPost['id'] != ''){
            $banner = Banner::findOne($this->dataPost['id']);
            $banner->created = date("Y-m-d H:i:s");
            $banner->user_id = $this->dataPost['uid'];
        }
        else{
            $banner = new Banner();
            $banner->created = date("Y-m-d H:i:s");
            $banner->user_id = $this->dataPost['uid'];
        }
        $banner->title = $this->dataPost['title'];
        $banner->link = (isset($this->dataPost['link']) ? $this->dataPost['link'] : null);
        $banner->vi_tri = floatval(str_replace(',','', $this->dataPost['vi_tri']));
        $banner->region = (isset($this->dataPost['region']['key']) ? $this->dataPost['region']['key'] : null) ;

        if($banner->save()){
            foreach ($this->dataPost['anhBanners'] as $index => $anhBanner){
                $arr_data_image = explode(',', $anhBanner);
                $file = base64_decode($arr_data_image[1]);
                $loai_file = explode('/', explode(';', $arr_data_image[0])[0])[1];
                $fileName = time().$index.myAPI::createCode($this->dataPost['title']).'.'.$loai_file;

                file_put_contents(dirname(dirname(__DIR__)).'/images/'.$fileName, $file);

                $imageBanner = new ImagesBanner();
                $imageBanner->banner_id = $banner->id;
                $imageBanner->image = $fileName;

                $imageBanner->save();
            }

            return [
                'content' => 'Đã lưu thông tin banner'
            ];
        }
        else
            throw new HttpException(500, Html::errorSummary($banner));
    }

    /** delete */
    public function actionDelete(){
        $dataPost = myAPI::getDataPost();
        Banner::updateAll(['active' => 0], ['id' => $dataPost['danh_muc']]);
        return [
            'content' => 'Đã xóa dữ liệu thành công',

        ];
    }

    //xoa-anh-banner
    public function actionXoaAnhBanner(){
        if(isset($this->dataPost['hinhAnh'])){
            $model = ImagesBanner::findOne($this->dataPost['hinhAnh']);
            $model->delete();
        }
        return [
            'content' => 'Đã xóa hình ảnh thành công',
        ];
    }

}
