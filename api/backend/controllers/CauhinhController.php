<?php

namespace backend\controllers;

use backend\models\Cauhinh;
use common\models\myAPI;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class CauhinhController extends CoreApiController
{
    public function behaviors()
    {
        $arr_action = ['get-data', 'save'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name = strtolower(str_replace('action', '', $action->id));
                    $data = myAPI::getDataPost();
                    $uid = $data['uid'];
                    return myAPI::isAccess2('Cauhinh', $action_name, $uid);
                },
            ];
        }

        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /** get-data */
    public function actionGetData()
    {
        $data = ArrayHelper::map(
            Cauhinh::find()->all(),
            'ghi_chu',
            'content'
        );
        return $data;
    }

    /** save */
    public function actionSave()
    {
        $chinh_sach_nhan_tien = Cauhinh::findOne(['ghi_chu' => 'chinh_sach_nhan_tien']);
        $chinh_sach_nhan_tien->updateAttributes(['content' => $this->dataPost['chinh_sach_nhan_tien']]);

        return [
            'title' => 'Lưu cấu hình',
            'content' => 'Đã lưu cấu hình thành công',
        ];
    }
}
