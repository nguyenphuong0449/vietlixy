<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%banner}}".
 *
 * @property int $id
 * @property string|null $title
 * @property int|null $active
 * @property int|null $user_id
 * @property int|null $user_updated_id
 * @property string|null $link
 * @property string|null $updated
 * @property string|null $region
 * @property string|null $created
 * @property float|null $vi_tri
 *
 * @property ImagesBanner[] $imagesBanners
 */
class Banner extends \yii\db\ActiveRecord
{
  const DAU_TRANG = 'Đầu trang';
  const GIUA_TRANG = 'Giữa trang';
  const CUOI_TRANG = 'Cuối trang';

    public $anhBanners = [];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%banner}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['region', 'created'], 'string'],
            [['vi_tri'], 'number'],
            [['active', 'user_id', 'anhBanners', 'updated', 'user_updated_id'], 'safe'],
            [['title', 'link'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'link' => 'Link',
            'region' => 'Region',
            'vi_tri' => 'Vi Tri',
        ];
    }

    /**
     * Gets query for [[ImagesBanners]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImagesBanners()
    {
        return $this->hasMany(ImagesBanner::className(), ['banner_id' => 'id']);
    }
}
