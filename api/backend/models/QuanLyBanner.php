<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%quan_ly_banner}}".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $link
 * @property string|null $region
 * @property float|null $vi_tri
 * @property string|null $image
 */
class QuanLyBanner extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%quan_ly_banner}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['region'], 'string'],
            [['vi_tri'], 'number'],
            [['title', 'link', 'image'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'link' => 'Link',
            'region' => 'Region',
            'vi_tri' => 'Vi Tri',
            'image' => 'Image',
        ];
    }

}
