<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string $ma_chien_dich
 * @property string|null $title
 * @property string|null $track_link
 * @property string|null $preview_offer
 * @property string|null $image
 * @property float|null $hoa_hong_tu_net
 * @property string|null $type_hoa_hong
 * @property float|null $Capped
 * @property int|null $Request
 * @property string|null $conversion_requirements
 * @property int|null $noi_bat
 * @property float|null $vi_tri_hien_thi
 * @property int|null $active
 * @property string|null $trang_thai
 * @property string|null $network
 * @property string|null $category
 * @property string|null $type
 * @property string|null $devide
 * @property string|null $ho_ten
 */
class QuanLyChienDich extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%quan_ly_chien_dich}}';
    }

    public function rules()
    {
        return [
            [['ma_chien_dich'], 'required'],
            [['track_link', 'conversion_requirements'], 'string'],
            [['hoa_hong_tu_net', 'Capped', 'vi_tri_hien_thi'], 'number'],
            [['Request', 'noi_bat', 'active'], 'integer'],
            [['ma_chien_dich'], 'string', 'max' => 30],
            [['title', 'preview_offer', 'image'], 'string', 'max' => 200],
            [['type_hoa_hong', 'trang_thai'], 'string'],
            [['network', 'category', 'type', 'devide'], 'string', 'max' => 100],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'ma_chien_dich' => 'Ma Chien Dich',
            'title' => 'Title',
            'track_link' => 'Track Link',
            'preview_offer' => 'Preview Offer',
            'image' => 'Image',
            'hoa_hong_tu_net' => 'Hoa Hong Tu Net',
            'type_hoa_hong' => 'Type Hoa Hong',
            'Capped' => 'Capped',
            'Request' => 'Request',
            'conversion_requirements' => 'Conversion Requirements',
            'noi_bat' => 'Noi Bat',
            'vi_tri_hien_thi' => 'Vi Tri Hien Thi',
            'active' => 'Active',
            'trang_thai' => 'Trạng thái',
            'network' => 'Network',
            'category' => 'Category',
            'type' => 'Type',
            'devide' => 'Devide',
        ];
    }
}
