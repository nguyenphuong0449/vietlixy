<?php

namespace backend\models;

use common\models\myAPI;
use Yii;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string $name
 * @property string $type
 * @property int $active
 * @property int $noi_bat
 * @property int $vi_tri
 * @property string|null $code
 * @property string|null $ghi_chu
 * @property string|null $image
 * @property string|null $link
 * @property float|null $ti_le_hoan_tien
 *
 */
class DanhMuc extends ActiveRecord
{
    //enum('Khu vực', '', '', '', )
    const COUNTRY = 'Offer Country';
    const CATEGORY = 'Category';
    const OFFER_TYPE = 'Offer type';
    const OFFER_DEVICE = 'Offer Device';
    const THUONG_HIEU = 'Thương hiệu';
    const DICH_VU = 'Dịch vụ';
    const KHU_VUC = 'Khu vực';
    const TEN_NGAN_HANG = 'Tên ngân hàng';
    const FANPAGE = 'Fanpage Facebook';
    const Zalo = 'Zalo';

    public static function tableName()
    {
        return '{{%danh_muc}}';
    }

    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'ghi_chu'], 'string'],
            [['name', 'code'], 'string', 'max' => 100],
            [['active', 'image', 'ti_le_hoan_tien', 'noi_bat', 'vi_tri', 'link'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên',
            'type' => 'Phân loại',
            'code' => 'Code',
            'active' => 'Active',
            'ghi_chu' => 'Ghi Chu',
        ];
    }

    public function beforeSave($insert)
    {
        $this->code = myAPI::createCode($this->name);
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }
}
