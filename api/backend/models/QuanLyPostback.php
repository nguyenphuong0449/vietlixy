<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%quan_ly_postback}}".
 *
 * @property int $id
 * @property string $utm_source
 * @property int|null $status
 * @property string|null $reward
 * @property string|null $trang_thai
 * @property float|null $hoa_hong
 * @property string|null $type_hoa_hong
 * @property float|null $so_tien_nhan_duoc
 * @property float|null $loi_nhuan
 * @property string $created
 * @property string|null $updated
 * @property int|null $chien_dich_id
 * @property int|null $user_id
 * @property string|null $title
 * @property string|null $hoten
 * @property string|null $dien_thoai
 */
class QuanLyPostback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%quan_ly_postback}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'chien_dich_id', 'user_id'], 'integer'],
            [['utm_source', 'created'], 'required'],
            [['reward', 'trang_thai', 'type_hoa_hong'], 'string'],
            [['hoa_hong', 'so_tien_nhan_duoc', 'loi_nhuan'], 'number'],
            [['created', 'updated'], 'safe'],
            [['utm_source', 'dien_thoai'], 'string', 'max' => 20],
            [['title'], 'string', 'max' => 200],
            [['hoten'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'utm_source' => 'Utm Source',
            'status' => 'Status',
            'reward' => 'Reward',
            'trang_thai' => 'Trang Thai',
            'hoa_hong' => 'Hoa Hong',
            'type_hoa_hong' => 'Type Hoa Hong',
            'so_tien_nhan_duoc' => 'So Tien Nhan Duoc',
            'loi_nhuan' => 'Loi Nhuan',
            'created' => 'Created',
            'updated' => 'Updated',
            'chien_dich_id' => 'Chien Dich ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'hoten' => 'Hoten',
            'dien_thoai' => 'Dien Thoai',
        ];
    }
}
