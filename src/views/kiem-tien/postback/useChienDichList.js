import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'

import { useToast } from 'vue-toastification/composition'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'
import { getUserData } from '@/auth/utils'

export default function useChienDichList() {
  // Use toast
  const toast = useToast()
  const refPostBackChienDichListTable = ref(null)

  // Table Handlers
  const tableColumns = [
    { key: 'id', label: 'STT', thStyle: { width: '1%' } },
    { key: 'image', label: 'Image' },
    { key: 'title', label: 'Tên chiến dịch' },
    { key: 'so_tien_nhan_duoc', label: 'Tiền nhận được' },
    { key: 'ngay_thuc_hien', label: 'Ngày thực hiện' },
    { key: 'note_status', label: 'Trạng thái' },
    { key: 'hoten', label: 'Người thực hiện' },
    { key: 'actions', label: 'Tác vụ', thStyle: { width: '10%' } },
  ]
  const perPage = ref(20)
  const totalChienDichs = ref(0)
  const currentPage = ref(1)
  const perPageOptions = [10, 25, 50, 100]
  const searchQuery = ref('')
  const sortBy = ref('id')
  const isSortDirDesc = ref(true)
  const roleFilter = ref(null)
  const planFilter = ref(null)
  const statusFilter = ref(null)
  const fieldsTimKiem = ref([])
  const isViewAllUser = ref(false)
  const tongNetData = ref(false)
  const tongNhanDuocData = ref(false)

  const dataMeta = computed(() => {
    const localItemsCount = refPostBackChienDichListTable.value ? refPostBackChienDichListTable.value.localItems.length : 0
    return {
      from: perPage.value * (currentPage.value - 1) + (localItemsCount ? 1 : 0),
      to: perPage.value * (currentPage.value - 1) + localItemsCount,
      of: totalChienDichs.value,
    }
  })

  const refetchData = () => {
    refPostBackChienDichListTable.value.refresh()
  }

  watch(
    [currentPage, perPage, searchQuery, roleFilter, planFilter, statusFilter],
    () => {
      refetchData()
    },
  )
  const fetchChienDichs = (ctx, callback) => {
    const userData = getUserData()

    store
      .dispatch('app-chien-dich/fetchPostback', {
        q: searchQuery.value,
        perPage: perPage.value,
        page: currentPage.value,
        sortBy: sortBy.value,
        sortDesc: isSortDirDesc.value,
        role: roleFilter.value,
        plan: planFilter.value,
        status: statusFilter.value,
        offset: currentPage.value,
        limit: perPage.value,
        auth: userData.auth_key,
        uid: userData.id,
        fieldsSearch: fieldsTimKiem,
      })
      .then(response => {
        const {
          results,
          rows,
          isViewAll,
          tongNet,
          tongNhanDuoc,
        } = response.data

        callback(results)
        totalChienDichs.value = rows
        tongNetData.value = tongNet
        tongNhanDuocData.value = tongNhanDuoc
        isViewAllUser.value = isViewAll
      })
      .catch(() => {
        toast({
          component: ToastificationContent,
          props: {
            title: 'Error fetching users list',
            icon: 'AlertTriangleIcon',
            variant: 'danger',
          },
        })
      })
  }

  // *===============================================---*
  // *--------- UI ---------------------------------------*
  // *===============================================---*

  return {
    fieldsTimKiem,
    fetchChienDichs,
    tableColumns,
    perPage,
    currentPage,
    totalChienDichs,
    dataMeta,
    perPageOptions,
    searchQuery,
    sortBy,
    isSortDirDesc,
    refPostBackChienDichListTable,
    isViewAllUser,
    tongNetData,
    tongNhanDuocData,

    refetchData,

    // Extra Filters
    roleFilter,
    planFilter,
    statusFilter,
  }
}
