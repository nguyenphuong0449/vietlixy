import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'

// import axios from 'axios'
// Notification
import { useToast } from 'vue-toastification/composition'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'
import { getUserData } from '@/auth/utils'

export default function useSanPhamList() {
  // Use toast
  const toast = useToast()
  const refUserListTable = ref(null)
  const refListImageBanner = ref(null)

  // Table Handlers
  const tableColumns = [
    { key: 'id', label: 'STT', thStyle: { width: '1%' } },
    { key: 'anh_dai_dien', label: 'Hình ảnh', thStyle: { width: '1%' } },
    { key: 'name', label: 'Tên' },
    {
      key: 'gia_tri', sortable: true, label: 'Giá trị', thStyle: { width: '10%' },
    },
    { key: 'actions', label: 'Tác vụ', thStyle: { width: '10%' } },
  ]
  const perPage = ref(10)
  const totalUsers = ref(0)
  const currentPage = ref(1)
  const perPageOptions = [10, 25, 50, 100]
  const searchQuery = ref('')
  const sortBy = ref('id')
  const isSortDirDesc = ref(true)
  const roleFilter = ref(null)
  const planFilter = ref(null)
  const statusFilter = ref(null)
  const fieldsTimKiem = ref([])

  const dataMeta = computed(() => {
    const localItemsCount = refUserListTable.value ? refUserListTable.value.localItems.length : 0
    return {
      from: perPage.value * (currentPage.value - 1) + (localItemsCount ? 1 : 0),
      to: perPage.value * (currentPage.value - 1) + localItemsCount,
      of: totalUsers.value,
    }
  })

  const refetchData = () => {
    refUserListTable.value.refresh()
  }
  const currentUser = getUserData()

  watch([currentPage, perPage, searchQuery, roleFilter, planFilter, statusFilter], () => {
    refetchData()
  })
  const fetchAllData = (ctx, callback) => {
    const userData = getUserData()

    store
      .dispatch('app-qua-tang/fetchGetAllData', {
        q: searchQuery.value,
        perPage: perPage.value,
        page: currentPage.value,
        sortBy: sortBy.value,
        sortDesc: isSortDirDesc.value,
        role: roleFilter.value,
        plan: planFilter.value,
        status: statusFilter.value,
        offset: currentPage.value,
        limit: perPage.value,
        auth: userData.auth_key,
        uid: userData.id,
        fieldsSearch: fieldsTimKiem,
      })
      .then(response => {
        const { results, rows } = response.data

        callback(results)
        totalUsers.value = rows
      })
      .catch(() => {
        toast({
          component: ToastificationContent,
          props: {
            title: 'Error fetching users list',
            icon: 'AlertTriangleIcon',
            variant: 'danger',
          },
        })
      })
  }

  const xoaKhachHang = (id, callback, before, after, showToast) => {
    before()
    store
      .dispatch('app-qua-tang/delete', {
        auth: currentUser.auth_key,
        uid: currentUser.id,
        danh_muc: id,
      })
      .then(response => {
        callback()
        after()
        showToast(response.data.content, 'success')
      })
      .catch(e => {
        after()
        showToast(e.message, 'danger')
      })
  }

  const xoaAnhBanner = (id, callback, before, after, showToast) => {
    before()
    store
      .dispatch('app-qua-tang/xoaAnhQuaTang', {
        auth: currentUser.auth_key,
        uid: currentUser.id,
        hinhAnh: id,
      })
      .then(response => {
        callback(response)
        after()
        showToast(response.data.content, 'success')
      })
      .catch(e => {
        after()
        showToast(e.message, 'danger')
      })
  }

  return {
    fieldsTimKiem,
    fetchAllData,
    xoaAnhBanner,
    xoaKhachHang,
    tableColumns,
    perPage,
    currentPage,
    totalUsers,
    dataMeta,
    perPageOptions,
    searchQuery,
    sortBy,
    isSortDirDesc,
    refUserListTable,
    refListImageBanner,

    refetchData,

    // Extra Filters
    roleFilter,
    planFilter,
    statusFilter,
  }
}
