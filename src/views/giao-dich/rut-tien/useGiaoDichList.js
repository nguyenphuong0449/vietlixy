import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'

// import axios from 'axios'
// Notification
import { useToast } from 'vue-toastification/composition'
import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'
import { getUserData } from '@/auth/utils'

export default function useGiaoDichList() {
// Use toast
  const toast = useToast()
  const refUserListTable = ref(null)
  const refUserTwoListTable = ref(null)

  // Table 1 Handlers
  const tableColumns = [
    { key: 'index', label: 'STT', thStyle: { width: '1%' } },
    { key: 'name', sortable: true, label: 'Tên quà tặng' },
    { key: 'hoten', sortable: true, label: 'Người thực hiện' },
    { key: 'so_luong', sortable: true, label: 'Số lượng' },
    { key: 'tong_tien', sortable: true, label: 'Số tiền' },
    { key: 'created', sortable: true, label: 'Ngày giao dịch' },
    { key: 'trang_thai', sortable: true, label: 'Trạng thái' },
    { key: 'ghi_chu', sortable: true, label: 'Ghi chú' },
    { key: 'actions', label: 'Tác vụ', thStyle: { width: '10%' } },
  ]
  // Table 2 Handlers
  const tableSeacondColumns = [
    { key: 'index', label: 'STT', thStyle: { width: '1%' } },
    { key: 'username', sortable: true, label: 'Người thực hiện' },
    { key: 'cost', sortable: true, label: 'Số tiền' },
    { key: 'created', sortable: true, label: 'Ngày giao dịch' },
    { key: 'bank_number', sortable: true, label: 'Số tài khoản' },
    { key: 'bank_name', sortable: true, label: 'Tên ngân hàng' },
    { key: 'trang_thai', sortable: true, label: 'Trạng thái' },
    { key: 'actions', label: 'Tác vụ', thStyle: { width: '10%' } },
  ]
  const perPageOne = ref(10)
  const perPageTwo = ref(10)
  const totalUsers = ref(0)
  const totalUsersTwo = ref(0)
  const currentPageOne = ref(1)
  const currentPageTwo = ref(1)
  const perPageOptions = [10, 25, 50, 100]
  const searchQueryOne = ref('')
  const searchQueryTwo = ref('')
  const sortBy = ref('id')
  const isSortDirDesc = ref(true)
  const roleFilterOne = ref(null)
  const roleFilterTwo = ref(null)
  const planFilterOne = ref(null)
  const planFilterTwo = ref(null)
  const statusFilterOne = ref(null)
  const statusFilterTwo = ref(null)
  const walletUser = ref(0)
  const isUpdateUser = ref(false)
  const fieldsTimKiem = ref([])
  const fieldsTimKiemTwo = ref([])
  const tongTienData = ref(null)
  const tongTienDataTwo = ref(null)

  const dataMeta = computed(() => {
    const localItemsCount = refUserListTable.value ? refUserListTable.value.localItems.length : 0
    return {
      from: perPageOne.value * (currentPageOne.value - 1) + (localItemsCount ? 1 : 0),
      to: perPageOne.value * (currentPageOne.value - 1) + localItemsCount,
      of: totalUsers.value,
    }
  })
  const dataMetaTwo = computed(() => {
    const localItemsTwoCount = refUserTwoListTable.value ? refUserTwoListTable.value.localItems.length : 0
    return {
      from: perPageTwo.value * (currentPageTwo.value - 1) + (localItemsTwoCount ? 1 : 0),
      to: perPageTwo.value * (currentPageTwo.value - 1) + localItemsTwoCount,
      of: totalUsersTwo.value,
    }
  })

  const refetchData = () => {
    refUserListTable.value.refresh()
  }
  const refetchTwoData = () => {
    refUserTwoListTable.value.refresh()
  }

  const currentUser = getUserData()

  watch([currentPageOne, perPageOne, searchQueryOne, roleFilterOne, planFilterOne, statusFilterOne], () => {
    refetchData()
  })
  watch([currentPageTwo, perPageTwo, searchQueryTwo, roleFilterTwo, planFilterTwo, statusFilterTwo], () => {
    refetchTwoData()
  })
  const fetchUsers = (ctx, callback) => {
    store
      .dispatch('app-rut-tien/fetchGiaoDich', {
        q: searchQueryOne.value,
        perPageOne: perPageOne.value,
        pageOne: currentPageOne.value,
        sortBy: sortBy.value,
        sortDesc: isSortDirDesc.value,
        role: roleFilterOne.value,
        plan: planFilterOne.value,
        status: statusFilterOne.value,
        offsetOne: currentPageOne.value,
        limitOne: perPageOne.value,
        auth: currentUser.auth_key,
        uid: currentUser.id,
        fieldsSearch: fieldsTimKiem,

      })
      .then(response => {
        const {
          results, rowOnes, total, isUpdate, tongTien,
        } = response.data

        callback(results)
        totalUsers.value = rowOnes
        walletUser.value = total
        tongTienData.value = tongTien
        isUpdateUser.value = isUpdate
      })
      .catch(() => {
        toast({
          component: ToastificationContent,
          props: {
            title: 'Lỗi lấy dữ liệu giao dịch',
            icon: 'AlertTriangleIcon',
            variant: 'danger',
          },
        })
      })
  }
  const fetchRutTien = (ctx, callback) => {
    store
      .dispatch('app-rut-tien/fetchRutTien', {
        q: searchQueryTwo.value,
        perPageTwo: perPageTwo.value,
        pageTwo: currentPageTwo.value,
        sortBy: sortBy.value,
        sortDesc: isSortDirDesc.value,
        role: roleFilterTwo.value,
        plan: planFilterTwo.value,
        status: statusFilterTwo.value,
        offsetTwo: currentPageOne.value,
        limitTwo: perPageTwo.value,
        auth: currentUser.auth_key,
        uid: currentUser.id,
        fieldsSearch: fieldsTimKiemTwo,
      })
      .then(response => {
        const {
          results, rowTwos, total, isUpdate, tongTienTwo,
        } = response.data

        callback(results)
        totalUsersTwo.value = rowTwos
        walletUser.value = total
        tongTienDataTwo.value = tongTienTwo
        isUpdateUser.value = isUpdate
      })
      .catch(() => {
        toast({
          component: ToastificationContent,
          props: {
            title: 'Lỗi lấy dữ liệu giao dịch',
            icon: 'AlertTriangleIcon',
            variant: 'danger',
          },
        })
      })
  }

  const xoaYeuCau = (id, callback, before, after, showToast) => {
    before()
    store
      .dispatch('app-rut-tien/delete', {
        auth: currentUser.auth_key,
        uid: currentUser.id,
        id,
      })
      .then(response => {
        callback()
        after()
        showToast(response.data.message, 'success')
      })
      .catch(e => {
        after()
        showToast(e.message, 'danger')
      })
  }

  const duyetGiaoDich = (id, callback, before, after, showToast) => {
    before()
    store
      .dispatch('app-rut-tien/confirm', {
        auth: currentUser.auth_key,
        uid: currentUser.id,
        id,
      })
      .then(response => {
        callback()
        after()
        showToast(response.data.message, 'success')
      })
      .catch(e => {
        after()
        showToast(e.message, 'danger')
      })
  }

  const huyGiaoDich = (id, callback, before, after, showToast) => {
    before()
    store
      .dispatch('app-rut-tien/cancel', {
        auth: currentUser.auth_key,
        uid: currentUser.id,
        id,
      })
      .then(response => {
        callback()
        after()
        showToast(response.data.message, 'success')
      })
      .catch(e => {
        after()
        showToast(e.message, 'danger')
      })
  }
  const xacNhanRutTien = (id, callback, before, after, showToast) => {
    before()
    store
      .dispatch('app-rut-tien/xacNhanRutTien', {
        auth: currentUser.auth_key,
        uid: currentUser.id,
        giaoDichID: id,
      })
      .then(response => {
        callback()
        after()
        showToast(response.data.content, 'success')
      })
      .catch(e => {
        callback()
        after()
        toast({
          component: ToastificationContent,
          props: {
            title: 'Thông báo',
            icon: 'BellIcon',
            variant: 'danger',
            text: e.response.data.message,
          },
        })
      })
  }
  const xacNhanDoiQua = (id, callback, before, after, showToast) => {
    before()
    store
      .dispatch('app-rut-tien/xacNhanDoiQua', {
        auth: currentUser.auth_key,
        uid: currentUser.id,
        quaTangID: id,
      })
      .then(response => {
        callback()
        after()
        showToast(response.data.content, 'success')
      })
      .catch(e => {
        callback()
        after()
        toast({
          component: ToastificationContent,
          props: {
            title: 'Thông báo',
            icon: 'BellIcon',
            variant: 'danger',
            text: e.response.data.message,
          },
        })
      })
  }
  // *===============================================---*
  // *--------- UI ---------------------------------------*
  // *===============================================---*

  return {
    fieldsTimKiem,
    fieldsTimKiemTwo,
    fetchUsers,
    currentPageTwo,
    fetchRutTien,
    tableColumns,
    tableSeacondColumns,
    perPageOne,
    perPageTwo,
    currentPageOne,
    totalUsers,
    totalUsersTwo,
    walletUser,
    dataMeta,
    dataMetaTwo,
    perPageOptions,
    searchQueryOne,
    searchQueryTwo,
    sortBy,
    isSortDirDesc,
    refUserListTable,
    refetchData,
    refUserTwoListTable,
    xoaYeuCau,
    isUpdateUser,
    // Extra Filters
    roleFilterOne,
    roleFilterTwo,
    planFilterOne,
    planFilterTwo,
    statusFilterOne,
    statusFilterTwo,
    huyGiaoDich,
    duyetGiaoDich,
    xacNhanRutTien,
    xacNhanDoiQua,
    refetchTwoData,
    tongTienData,
    tongTienDataTwo,
  }
}
